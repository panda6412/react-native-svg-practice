import React from 'react'
import { Animated } from 'react-native'
import Svg, { Text, LinearGradient, RadialGradient, Rect, Stop, Defs, TSpan, Polygon } from 'react-native-svg'

const SIX_EDGE_OF_HEXAGON = [17, 16, 17, 17, 16, 17]
const SQRT3 = Math.sqrt(3)

const EDGE_LENGTH = 60
const BASE_WIDTH = 4
const PROGRESS_WIDTH = 15
const TEXT_STYLE = { fontSize: 30, color: 'black' }

const px2mm = px => px * 0.352778

const transferPoints = points => points.map(({ x, y }) => `${x},${y}`).join(' ')

const points2Path = points => {
  const { x: firstPathX, y: firstPathY } = points[0]
  const remainingPath = points
    .filter((_, index) => index !== 0)
    .map(({ x, y }) => `L${x} ${y}`)
    .join(' ')
  return `M${firstPathX} ${firstPathY} ` + remainingPath + ' Z'
}

const getCenter = edgeLength => ({ x: (edgeLength * SQRT3) / 2, y: edgeLength })

class Hexagon extends React.Component {
  state = {
    animatedProgress: new Animated.Value(0)
  }

  componentDidMount () {
    const { percent, animate = false } = this.props
    const { animatedProgress } = this.state
    console.log('this', this._svg)
    if (percent < 0 || percent > 100) throw 'Argument "percent" must in the range (0~100)'
    animate ? this._animate() : animatedProgress.setValue(percent)
  }

  shouldComponentUpdate (nextProps) {
    const { percent } = this.props
    return nextProps.percent !== percent
  }

  componentDidUpdate () {
    const { percent, animate = false } = this.props
    const { animatedProgress } = this.state
    if (percent < 0 || percent > 100) throw 'Argument "percent" must in the range (0~100)'
    animate ? this._animate() : animatedProgress.setValue(percent)
  }

  scalePoints (points, scale) {
    const { edgeLength = 60 } = this.props
    const { x: centerX, y: centerY } = getCenter(edgeLength)
    return points.map(({ x, y }) => ({
      x: ((x - centerX) * scale + x).toFixed(2),
      y: ((y - centerY) * scale + y).toFixed(2)
    }))
  }

  getOutputRange () {
    const anglePercent = [0, 17, 33, 50, 67, 83, 100]
    return anglePercent.map(percent => {
      const progressBasePoints = this.generateProgressBasePoints(percent)
      const progrssPoints = this.getHexagonProgrssPoints(progressBasePoints)
      const progrssPointsLength = progrssPoints.length
      const shouldFillLength = 14 - progrssPointsLength
      const centerLeftPoints = progrssPoints[progrssPointsLength / 2 - 1]
      const centerRightPoints = progrssPoints[progrssPointsLength / 2]
      const tempRemovedPoints = progrssPoints.splice(progrssPointsLength / 2)

      // Fill up to 14 elements
      for (let i = 1; i <= shouldFillLength; i++) {
        i <= shouldFillLength / 2 ? progrssPoints.push(centerLeftPoints) : progrssPoints.push(centerRightPoints)
      }

      const concatedPoints = progrssPoints.concat(tempRemovedPoints)
      return transferPoints(concatedPoints)
    })
  }

  findIndexOfEdgeRecursive (percentage, sumOfEdge = 0, index = 0) {
    sumOfEdge += SIX_EDGE_OF_HEXAGON[index]
    if (sumOfEdge >= percentage) {
      const remaingEdge = sumOfEdge === percentage ? 0 : SIX_EDGE_OF_HEXAGON[index] - (sumOfEdge - percentage)
      return { index, remaingEdge }
    }
    return this.findIndexOfEdgeRecursive(percentage, sumOfEdge, ++index)
  }

  getbaseHexagonPoints (edgeLength) {
    const ponint1 = { x: (edgeLength * SQRT3) / 2, y: 0 }
    const ponint2 = { x: edgeLength * SQRT3, y: edgeLength / 2 }
    const ponint3 = { x: edgeLength * SQRT3, y: (edgeLength * 3) / 2 }
    const ponint4 = { x: (edgeLength * SQRT3) / 2, y: edgeLength * 2 }
    const ponint5 = { x: 0, y: (edgeLength * 3) / 2 }
    const ponint6 = { x: 0, y: edgeLength / 2 }

    return [ponint1, ponint2, ponint3, ponint4, ponint5, ponint6]
  }

  /**
   * 根據趴數找到在 base polygon 上對應 progress bar 的基準點
   *
   * @param {number} percentage
   * @return progressPoints - progress 的基準點
   */
  generateProgressBasePoints (percentage) {
    const { edgeLength = EDGE_LENGTH } = this.props
    const basePoints = this.getbaseHexagonPoints(edgeLength)

    if (percentage === 0) return [basePoints[0], basePoints[0]]

    if (percentage === 100) {
      basePoints.push(basePoints[0])
      return basePoints
    }

    const { index: indexOfEdge, remaingEdge } = this.findIndexOfEdgeRecursive(percentage)
    const NUMBER_OF_DOTS = indexOfEdge + 2

    const progressPoints = basePoints.filter((_, index) => index < NUMBER_OF_DOTS)

    // 如果剩下的邊長不是剛好在頂點上，需要再另外處理
    if (remaingEdge !== 0) {
      const endPoints = progressPoints.pop()
      const startPoints = progressPoints.pop()

      // 內分點計算
      const RATIO_A = remaingEdge
      const RATIO_B = SIX_EDGE_OF_HEXAGON[indexOfEdge] - remaingEdge

      const x = ((startPoints.x * RATIO_B + endPoints.x * RATIO_A) / (RATIO_A + RATIO_B)).toFixed(2)
      const y = ((endPoints.y * RATIO_A + startPoints.y * RATIO_B) / (RATIO_A + RATIO_B)).toFixed(2)

      if (indexOfEdge !== 5) {
        progressPoints.push(startPoints)
        progressPoints.push({ x, y })
      } else {
        progressPoints.push(startPoints)
        progressPoints.push(endPoints)

        const theFirstPoint = progressPoints[0]
        const x = ((endPoints.x * RATIO_B + theFirstPoint.x * RATIO_A) / (RATIO_A + RATIO_B)).toFixed(2)
        const y = ((theFirstPoint.y * RATIO_A + endPoints.y * RATIO_B) / (RATIO_A + RATIO_B)).toFixed(2)
        progressPoints.push({ x, y })
      }
    }

    return progressPoints
  }

  /**
   * 根據 base progress 的基準點向外延伸即向內縮小，將這兩條線合併在一起即是 progress polygon
   *
   * @param {number} percentage
   * @return progressPolygon - 完整的 progress bar polygon
   */
  getHexagonProgrssPoints (progressBasePoints) {
    const { edgeLength = EDGE_LENGTH, progressWidth = PROGRESS_WIDTH } = this.props
    const halfOfProgressWidth = progressWidth / 2

    const scaleUpRatio = halfOfProgressWidth / (edgeLength + halfOfProgressWidth)
    const scaleDownRatio = -halfOfProgressWidth / (edgeLength + halfOfProgressWidth)

    const outerProgressPoints = this.scalePoints(progressBasePoints, scaleUpRatio)
    const innerProgressPoints = this.scalePoints(progressBasePoints, scaleDownRatio).reverse()
    return [...outerProgressPoints, ...innerProgressPoints]
  }

  _animate () {
    const { percent, duration = 50000 } = this.props
    const { animatedProgress } = this.state
    Animated.timing(animatedProgress, {
      toValue: percent,
      duration: duration
    }).start()
  }

  render () {
    const {
      percent = 0,
      edgeLength = EDGE_LENGTH,
      baseWidth = BASE_WIDTH,
      progressWidth = PROGRESS_WIDTH,
      textStyle = TEXT_STYLE
    } = this.props

    const { animatedProgress } = this.state

    const halfOfProgressWidth = progressWidth / 2
    const basePoints = this.getbaseHexagonPoints(edgeLength)
    const basePointsStr = transferPoints(basePoints)
    const { x: centerX, y: centerY } = getCenter(edgeLength)
    const { fontSize, color } = textStyle
    const percentFontSize = fontSize - 10

    const PERCENT_CHARACTER_LENGTH = percent.toString().length

    const points = animatedProgress.interpolate({
      inputRange: [0, 17, 33, 50, 67, 83, 100],
      outputRange: this.getOutputRange()
    })

    const svgHeight = edgeLength * 2 + progressWidth
    const svgWidth = SQRT3 * edgeLength + progressWidth

    const AnimatedPolygon = Animated.createAnimatedComponent(Polygon)
    return (
      <>
        <Svg height='300' width='300'>
          <Defs>
            <LinearGradient id='a1' x1='0' y1='1' x2='0' y2='0'>
              <Stop offset='0%' stopColor='#0c0' />
              <Stop offset='100%' stopColor='#fff' />
            </LinearGradient>
          </Defs>
          <Rect width='70' height='70' x='30' y='50' stroke='#000' fill='url(#a1)' />
        </Svg>
        <Svg
          ref={svg => {
            console.log('svg', svg)
            this._svg = svg
            this.setState({ svg: svg })
          }}
          height={svgHeight}
          width={svgWidth}
          viewBox={`-${halfOfProgressWidth} -${halfOfProgressWidth} ${svgWidth} ${svgHeight}`}
        >
          <Polygon points={basePointsStr} fill='none' stroke='#E3E3E3' strokeWidth={baseWidth} />
          <AnimatedPolygon points={points} fill='url(#grad)' />
          <Text
            fill={color}
            stroke={color}
            fontSize={fontSize}
            x={centerX - px2mm(fontSize) + 1}
            y={centerY + px2mm(fontSize)}
            fontWeight='300'
            textAnchor='middle'
          >
            <TSpan>{percent}</TSpan>
            <TSpan
              fontFamily='PingFangTC-Medium'
              x={centerX + px2mm(percentFontSize) * PERCENT_CHARACTER_LENGTH + 3}
              fontWeight='200'
              fontSize={percentFontSize}
            >
              %
            </TSpan>
          </Text>
          <Defs>
            {/* <LinearGradient id='grad' x1='50%' y1='50%' x2='-25%' y2='-25%'> */}
            <LinearGradient id='grad' x1='1' y1='0' x2='0' y2='1'>
              <Stop offset='0' stopColor='rgb(255,232,120)' stopOpacity='1' />
              <Stop offset='1' stopColor='rgb(255,201,43)' stopOpacity='1' />
            </LinearGradient>
          </Defs>
        </Svg>
      </>
    )
  }
}

export default Hexagon
