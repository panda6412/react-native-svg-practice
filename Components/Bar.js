/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react'
import { SafeAreaView, StyleSheet, ScrollView, View, Text, StatusBar } from 'react-native'
import { LinearGradient, Stop, Defs } from 'react-native-svg'
import { ProgressCircle, BarChart, Grid, YAxis, XAxis } from 'react-native-svg-charts'

import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions
} from 'react-native/Libraries/NewAppScreen'

const data1 = [1000, 3120, 3500, 1500, 2480, 2780, 3150].map(value => ({
  value
}))
const data2 = [3150, 3600, 4500, 3215, 1000, 2100, 3320].map(value => ({
  value
}))

const barData = [
  {
    data: data1,
    svg: {
      fill: 'url(#grey-gradient)'
    }
  },
  {
    data: data2,
    svg: {
      fill: 'url(#yellow-gradient)'
    }
  }
]

const contentInset = { top: 20, bottom: 20 }

const GreyGradient = () => (
  <Defs key={'grey-gradient'}>
    <LinearGradient id={'grey-gradient'} x1={'0%'} y={'0%'} x2={'0%'} y2={'100%'}>
      <Stop offset={'0%'} stopColor={'rgb(130, 130, 130)'} />
      <Stop offset={'100%'} stopColor={'rgb(176, 176, 176)'} />
    </LinearGradient>
  </Defs>
)

const YellowGradient = () => (
  <Defs key={'yellow-gradient'}>
    <LinearGradient id={'yellow-gradient'} x1={'0%'} y={'0%'} x2={'0%'} y2={'100%'}>
      <Stop offset={'0%'} stopColor={'rgb(255, 201, 43)'} />
      <Stop offset={'100%'} stopColor={'rgb(255, 232, 120)'} />
    </LinearGradient>
  </Defs>
)

const Bar: () => React$Node = () => {
  return (
    <>
      <Text style={styles.sectionTitle}>Bar Chart</Text>
      <View style={{ height: 200, flexDirection: 'row' }}>
        <YAxis
          data={barData}
          contentInset={contentInset}
          svg={{
            fill: 'grey',
            fontSize: 10
          }}
          numberOfTicks={5}
          min={1000}
          max={5000}
          formatLabel={value => value}
        />
        <BarChart
          animate
          style={{ height: 200, flex: 1, marginLeft: 16 }}
          data={barData}
          yAccessor={({ item }) => item.value}
          contentInset={{ top: 20, bottom: 20, left: 16, right: 16 }}
          spacingInner={0.5}
        >
          <Grid />
          <GreyGradient />
          <YellowGradient />
        </BarChart>
      </View>
    </>
  )
}

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: Colors.lighter
  },
  engine: {
    position: 'absolute',
    right: 0
  },
  body: {
    backgroundColor: Colors.white
  },
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
    color: Colors.black
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
    color: Colors.dark
  },
  highlight: {
    fontWeight: '700'
  },
  footer: {
    color: Colors.dark,
    fontSize: 12,
    fontWeight: '600',
    padding: 4,
    paddingRight: 12,
    textAlign: 'right'
  }
})

export default Bar
