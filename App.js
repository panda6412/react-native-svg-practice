/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react'
import { SafeAreaView, StyleSheet, ScrollView, View, Text, StatusBar } from 'react-native'
import { LinearGradient, Stop, Defs } from 'react-native-svg'
import { ProgressCircle, BarChart, Grid, YAxis, XAxis } from 'react-native-svg-charts'

import Hexagon from './Components/Hexagon'
import Bar from './Components/Bar'

import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions
} from 'react-native/Libraries/NewAppScreen'

const data = [3020, 3120, 3500, 2500, 2480, 2780, 3150, 3600, 3758, 3215, 2988, 2100, 3320]

const data1 = [3020, 3120, 3500, 2500, 2480, 2780, 3150].map(value => ({
  value
}))
const data2 = [3150, 3600, 3758, 3215, 2988, 2100, 3320].map(value => ({
  value
}))

const barData = [
  {
    data: data1,
    svg: {
      fill: 'url(#grey-gradient)'
    }
  },
  {
    data: data2,
    svg: {
      fill: 'url(#yellow-gradient)'
    }
  }
]

const App: () => React$Node = () => {
  return (
    <>
      <StatusBar barStyle='dark-content' />
      <SafeAreaView>
        <ScrollView contentInsetAdjustmentBehavior='automatic' style={styles.scrollView}>
          <Header />
          {global.HermesInternal == null ? null : (
            <View style={styles.engine}>
              <Text style={styles.footer}>Engine: Hermes</Text>
            </View>
          )}
          <View style={styles.body}>
            <View style={styles.sectionContainer}>
              <Text style={styles.sectionTitle}>出貨進度</Text>
              <Hexagon percent={100} duration={10000} animate />
            </View>
            <View style={styles.sectionContainer}>
              <Bar />
            </View>

            {/* blank */}

            <View style={{ height: 200, flexDirection: 'row' }}>
              <Text style={styles.sectionTitle}>Something magic will happened here ...</Text>
            </View>
            <View style={styles.sectionContainer}>
              <Text style={styles.sectionTitle}>Learn More</Text>
              <Text style={styles.sectionDescription}>Read the docs to discover what to do next:</Text>
            </View>
            <LearnMoreLinks />
          </View>
        </ScrollView>
      </SafeAreaView>
    </>
  )
}

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: Colors.lighter
  },
  engine: {
    position: 'absolute',
    right: 0
  },
  body: {
    backgroundColor: Colors.white
  },
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
    color: Colors.black
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
    color: Colors.dark
  },
  highlight: {
    fontWeight: '700'
  },
  footer: {
    color: Colors.dark,
    fontSize: 12,
    fontWeight: '600',
    padding: 4,
    paddingRight: 12,
    textAlign: 'right'
  }
})

export default App
